// alert('hi')

const firstNum = 8 ** 2;
console.log(firstNum)

const secondNum = Math.pow(8, 2); //this is before ES6
console.log(secondNum)

// 5 raised to the power of 5
const thirdNum = 5 ** 5
console.log(thirdNum)

//Template literals
/*
	Allows to write strings without using the concatenation operator (+)

*/

let name = "Nehemiah"

// pre-template literal string
// Using single quote('')
let message = 'Hello ' + name + ' Welcome to programming!';
console.log('message without template literals: ' + message)

// strings using template literals
//Uses the backticks(``)

message = `Hello ${name}. Welcome to programming!`
console.log(message)


let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 **2 with the soluton of ${firstNum}`
console.log(anotherMessage)

anotherMessage = "\n" + name + ' attended a math competition. \n He won it by solving the problem 8 **2 with the solution of ' + firstNum
console.log(anotherMessage)

const interestRate = .1
const principal = 1000;
console.log(`The interest on your savince is: ${principal * interestRate}`);

// Array destructuring
/*
	Allows to unpack elements in arrays into distinct variables. ALlows us to name array elements with variables instead of index numbers

	Syntax:
		let/const [variableName, variableName, variableName] = array;

*/

const fullName = ["Joe","Dela","Cruz"]
// Pre-Array desctructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

//Array destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

// Object Destructuring
/*
	Allows us to unpack properties of objects into distinct variables. Shortens the syntax for accessing properties from objects

	syntax:
		let/const {propertyName, propertyName, propertyName}
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//Pre-object destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

const{maidenName, givenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)

// Arrow Functions
/*
	Alternative syntax to traditional functions

	const variableName = () => {
		console.log()
	}
*/

const hello = () =>{
	console.log('Hello World')
}

hello()

// traditional writing:
function greeting() {
	console.log("Hello world")
}

greeting()

// Pre-Arrow function
/*
	syntax :
		function functionName (parameterA, parameterB){
			console.log()
		}
*/

function printFullName (firstName,middleInitial,lastName) {
	console.log(firstName +" " + middleInitial+ " " + lastName)
}
printFullName("Amiel","D.","Oliva")

// ARROW FUNCTION
/*
	Syntax
		let/const variableName = (parameterA, parameterB) => {
			console.log()
		}
*/

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName1("Charles Patrick", "A.","Lilagan");

const students = ["Rupert", "Carlos", "Jerome"]
// ARROW FUNCTIONS WITH LOOPS
// pre-arrow function

students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// ARROW FUNCTION
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statement

// Pre-arrow Function
function add(x,y) {
	return x + y
}

let total = add(12, 15)
console.log(total)

// ARROW FUNCTIOn
const addition = (x, y) => x + y;

// const addition = (x, y) => {
// 	return x+y
// }

let resultOfAdd = addition(12, 15)
console.log(resultOfAdd)

// Default function arguement value

const greet = (name = "User") => {
	return `Good morning, ${name}`
}

console.log(greet())
console.log(greet('Grace'))

// Class-Based Object Blueprints
/*
	Allows creation/instatiation of objects using classes as blueprints

	syntax:
		class className{
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year
	}
}

const myCar = new Car();
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = 2021

console.log(myCar)

const myNewCar = new Car ('Toyota', 'Vios', 2021)
console.log(myNewCar)